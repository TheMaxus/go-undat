package dat

// Filetype enum
const (
	Unknown = iota
	DAT1
	DAT2
)

type Dat interface {
	PrintInfo()
	PrintFileList()
	Decompress()
	Close() error
}
