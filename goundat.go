package main

import (
	"fmt"
	"os"

	"gitlab.com/TheMaxus/goundat/dat"
	"gitlab.com/TheMaxus/goundat/dat1"
	"gitlab.com/TheMaxus/goundat/dat2"
)

func main() {
	args := os.Args[1:]

	if len(args) < 2 {
		help()
		return
	}
	file := open(args)
	switch args[0] {
	case "info":
		info(args[:], file)
	case "unpack":
		unpack(file)
	default:
		help()

	}
}

// Print help text
func help() {
	fmt.Println("Usage:\n",
		"	dat <command> <command options> <filname(s)>\n",
		"	info (-d: also print dirs): prints info about file\n",
		"	unpack: unpacks the file")
}

func open(args []string) dat.Dat {
	filename := args[len(args)-1]
	format := getFormat(filename)
	var file dat.Dat
	switch format {
	case dat.DAT1:
		file = dat1.Open(filename)
	case dat.DAT2:
		file = dat2.Open(filename)
	case dat.Unknown:
		fmt.Println("Unknown file format or file doesn't exist!")
		os.Exit(1)
	}
	return file
}

func unpack(d dat.Dat) {
	d.Decompress()
}

// This method trys to determine filetype automatically
// and calls a function that prints out filetype specific info
func info(args []string, d dat.Dat) {
	d.PrintInfo()
	if args[1] == "-d" {
		d.PrintFileList()
	}
}

// This method automatically determines the filetype
// by calling the validation functions from filetype specific
// packages
func getFormat(filename string) int {
	var format int

	file, err := os.Open(filename)
	if err != nil {
		fmt.Println(err)
		return dat.Unknown
	}
	defer file.Close()

	if dat2.ValidateFile(file) {
		format = dat.DAT2
	} else if dat1.ValidateFile(file) {
		format = dat.DAT1
	} else {
		format = dat.Unknown
	}
	return format
}
