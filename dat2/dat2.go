package dat2

import (
	"bufio"
	"compress/zlib"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/TheMaxus/goundat/dat"
)

// DAT2 datatypes
type File struct {
	file    *os.File
	header  header
	dirTree []dirEntry
}

type header struct {
	fileSize    uint32
	dirTreeSize uint32
	fileCount   uint32
}

type dirEntry struct {
	path           string
	compression    bool
	realSize       uint32
	compressedSize uint32
	offset         uint32
}

// This function reads the file tailheader and
// parses it into a Header object
func getHeader(f *os.File) header {
	const headerSize int64 = 8
	buffer := [headerSize]byte{}
	f.Seek(-headerSize, 2)

	nBytes, err := f.Read(buffer[:])
	if nBytes == 0 && err != io.EOF {
		panic(err)
	}

	fileSize := binary.LittleEndian.Uint32(buffer[4:])
	dirTreeSize := binary.LittleEndian.Uint32(buffer[:4])

	fileCountOffset := headerSize + int64(dirTreeSize)

	f.Seek(-fileCountOffset, 2)
	f.Read(buffer[:])
	fileCount := binary.LittleEndian.Uint32(buffer[:4])
	return header{
		fileSize:    fileSize,
		dirTreeSize: dirTreeSize,
		fileCount:   fileCount,
	}
}

// Returns directory tree offset
func dirTreeOffset(h header) uint32 {
	const headerSize int64 = 8
	fileCountOffset := headerSize + int64(h.dirTreeSize)
	return h.fileSize - uint32(fileCountOffset-4)
}

// This method opens the file and parses some information about it
func Open(filename string) dat.Dat {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}

	header := getHeader(file)

	return File{
		file:    file,
		header:  header,
		dirTree: getDirTree(file, header),
	}
}

func (d File) Close() error {
	return d.file.Close()
}

// This method validates the file by comparing the first 16 bits of
// the file to a magic number and checking that the last 32 bit
// integer in the end of the file matches the size of the file
func ValidateFile(file *os.File) bool {
	var tail [4]byte
	file.Seek(-4, 2)

	nBytes, err := file.Read(tail[:])
	if nBytes == 0 {
		panic(err)
	}

	size := binary.LittleEndian.Uint32(tail[:])
	fileStat, err := file.Stat()
	if err != nil {
		fmt.Printf("%v", err)
	}
	return int64(size) == fileStat.Size()
}

// Read the directory entry bytes and parse them into a dirEntry object
func parseDirEntry(nameLen uint32, buffer []byte) dirEntry {
	return dirEntry{
		path:           string(buffer[:nameLen]),
		compression:    buffer[nameLen] == 1,
		realSize:       binary.LittleEndian.Uint32(buffer[nameLen+1 : nameLen+5]),
		compressedSize: binary.LittleEndian.Uint32(buffer[nameLen+5 : nameLen+9]),
		offset:         binary.LittleEndian.Uint32(buffer[nameLen+9:]),
	}
}

// This function parses the directory tree of the file
func getDirTree(f *os.File, h header) []dirEntry {
	var dirTree []dirEntry
	buffer := [512]byte{}
	var nameLen uint32
	f.Seek(int64(dirTreeOffset(h)), 0)
	for i := 0; i < int(h.fileCount); i++ {
		// First read the length of the file path and it it's larger than 512 bytes
		// we throw error
		f.Read(buffer[:4])
		nameLen = binary.LittleEndian.Uint32(buffer[:4])
		if nameLen > 512 {
			fmt.Println("Error! Header too large!")
			break
		}
		// Then read the directory info and parse it
		f.Read(buffer[:nameLen+13])
		dirTree = append(dirTree, parseDirEntry(nameLen, buffer[:nameLen+13]))
	}
	return dirTree
}

// Pretty print the file information
func (d File) PrintInfo() {
	fmt.Printf("DAT2: %s\n	Directory Tree Size: %d \n	File Count: %d\n", d.file.Name(), d.header.dirTreeSize, d.header.fileCount)
}

// Decompresses all files
func (f File) Decompress() {
	for _, dir := range f.dirTree {
		f.decompressFile(dir)
	}
}

// Decompresses selected file
func (f File) decompressFile(d dirEntry) {
	outPath := strings.Join(strings.Split(d.path, "\\"), string(os.PathSeparator))
	os.MkdirAll(filepath.Dir(outPath), 0755)
	outFile, err := os.Create(outPath)
	if err != nil {
		log.Fatal(err.Error())
	}

	f.file.Seek(int64(d.offset), 0)
	log.Printf("Writing: %s", outPath)
	reader := bufio.NewReader(io.LimitReader(f.file, int64(d.compressedSize)))
	writer := bufio.NewWriter(outFile)
	var decoder io.Reader = reader
	if d.compression {
		r, err := zlib.NewReader(reader)
		decoder = r
		if err != nil {
			log.Fatal(err.Error())
		}
		defer r.Close()
	}
	writer.ReadFrom(decoder)
	writer.Flush()
}

// Print directory tree
func (d File) PrintFileList() {
	fmt.Println("Directories:")
	for _, dir := range d.dirTree {
		fmt.Printf("	%s\n", dir.path)
	}
}
