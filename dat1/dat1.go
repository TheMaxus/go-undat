package dat1

import (
	"encoding/binary"
	"fmt"
	"io"
	"os"

	"gitlab.com/TheMaxus/goundat/dat"
)

// DAT1 datatypes
type File struct {
	file   *os.File
	header header
	dirs   []directory
}

type header struct {
	dirCount uint32
	fileType uint32
	zero     uint32
	checkSum uint32
}

type directory struct {
	name      string
	fileCount uint32
	var1      uint32
	var2      uint32
	var3      uint32
	contents  []fileHeader
}

type fileHeader struct {
	name           string
	attributes     uint32
	offset         uint32
	originalSize   uint32
	compressedSize uint32
	// File start = offset, file end = offset + compressedSize
}

// This method opens the file and parses some information about it
func Open(filename string) dat.Dat {

	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	header := getHeader(file)
	dirs := getFolders(file, header)

	return File{
		file:   file,
		header: header,
		dirs:   dirs,
	}
}

func (f File) Close() error {
	return f.file.Close()
}

// This method returns the size of directory block
func (d File) dirBlockSize() int {
	dirBlockSize := 0
	for _, dir := range d.dirs {
		dirBlockSize += len(dir.name)
	}
	dirBlockSize += len(d.dirs)
	return dirBlockSize
}

// typeString returns filetype as a string for printing
func (d header) typeString() string {
	switch d.fileType {
	case 0x0A:
		return "Other"
	case 0x5E:
		return "master.dat"
	default:
		return "Unknown type"
	}
}

// getFolders parses the directory block and
// populates the dirs struct
// TODO: reimplement this using bufio
func getFolders(f *os.File, h header) []directory {
	f.Seek(16, 0)
	dirCount := int(h.dirCount)
	bytes := [512]byte{}
	var dirs []directory

	nBytes, err := f.Read(bytes[:])
	if nBytes == 0 {
		panic(err)
	}

	dir := directory{
		name:      "",
		fileCount: 0,
		var1:      0,
		var2:      0,
		var3:      0,
		contents:  []fileHeader{},
	}

	i := 0

	for i < nBytes && len(dirs) < dirCount {
		nameLength := int(bytes[i])
		i++

		// If end of the buffer contains only a part of dir string, copy partial string to dir, load new buffer into memory
		// and append the remainder of the string to dir
		if i+nameLength >= len(bytes) {
			dir.name = string(bytes[i:])
			nameLength -= len(dir.name)

			nBytes, err := f.Read(bytes[:])
			if nBytes == 0 && err != io.EOF {
				panic(err)
			}
			dir.name += string(bytes[:nameLength])
			i = 0
		} else {
			dir.name = string(bytes[i : i+nameLength])
		}

		dirs = append(dirs[:], dir)
		i += nameLength
	}
	return dirs
}

// getHeader reads first 16 bytes of the file and parses them into a header object
func getHeader(f *os.File) header {
	const headerSize int64 = 16
	buffer := [headerSize]byte{}

	nBytes, err := f.ReadAt(buffer[:], 0)

	if nBytes == 0 {
		panic(err)
	}

	return header{
		binary.BigEndian.Uint32(buffer[:4]),
		binary.BigEndian.Uint32(buffer[4:8]),
		binary.BigEndian.Uint32(buffer[8:12]),
		binary.BigEndian.Uint32(buffer[12:16])}

}

// TODO
func (d File) getDirContent(file *os.File, h header, dirs []directory) []fileHeader {
	panic("Not yet implemented!")
}

// DAT1 validation function.
// Checks if bytes 8 to 12 are zeroes in the file
func ValidateFile(file *os.File) bool {
	const dat1Magic uint32 = 0
	head := [16]byte{}

	nBytes, err := file.ReadAt(head[:], 0)
	if nBytes == 0 {
		panic(err)
	}

	magic := binary.BigEndian.Uint32(head[8:12])
	return dat1Magic == magic
}

func (f File) Decompress() {
	panic("Not yet implemented")
}

// PrintInfo prints out the file information to terminal
func (d File) PrintInfo() {
	fmt.Printf("DAT1: %s\n	Directories: %d\n	DAT type: %s\n	Checksum: 0x%X\n",
		d.file.Name(), d.header.dirCount, d.header.typeString(), d.header.checkSum)
}

// PrintFileList prints out directory listing
func (d File) PrintFileList() {
	fmt.Println("Directories:")
	for _, dir := range d.dirs {
		fmt.Printf("	%s\n", dir.name)
	}
}
